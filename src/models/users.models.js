const usuario = [
    {   
        id:0,
        email: "camila@gmail.com",
        nombre: "Camila Manchola",
        cel: 3124398310,
        direccion: "Cra 7 #2-99 sur",
        password: "12345",
        isAdmin: true
    },
];

const allusuario = () => {
    return usuario;
}

const pushusuario = (email,nombre, cel, direccion, password) => {
    const validarusuario = usuario[usuario.length-1].id+1;
    const usuarionuevo = {   
            id: usuario.length,
            email: email,
            nombre: nombre,
            cel: cel,
            direccion:direccion,
            password: password,
            isAdmin: false
        }
    
    usuario.push(usuarionuevo);
    
    return usuario
};

const putusuario = (id,email,nombre, cel,direccion, password) => {
    const filter = usuario.find( u => u.id == id)
    filter.email = email,
    filter.nombre = nombre;
    filter.cel = cel;
    filter.direccion =direccion;
    filter.password = password
    return filter
}

module.exports = {allusuario, pushusuario, putusuario}
