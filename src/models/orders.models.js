const { request } = require("express");

const {AllProductos} = require("./products.models");
const {allusuario} = require("./users.models");

const order = [
    {   
        id:0,
        nombre: "Camila Manchola",
        usuarionnombre: "camila@gmail.com",
        order: [],
        orderCost: 0,
        direccion: "Cra 7 # 2 - 99 sur",

        cel :3124398310,
        pago: 'Efectivo',
        estado: 'Pendiente'
    }
];
const allOrders = () => {
    return order;
}

const newOrder = (order) => {
    return order.push(order)
}

const usuarionUpdate = (id,nombre, usuarionnombre,order,orderCost,direccion,cel,pago,estado) => {
  return  {   
        id:id,
        nombre:nombre,
        usuarionnombre: usuarionnombre,
        order: order,
        orederCost: orderCost,
        direccion: direccion,
        cel: cel,
        pago: pago,
        estado: estado
    }
}
const newusuarionModel = (email,direccion,cel) => {
    const usuarion = allusuario().find( u => u.email == email);
    return order.push( {   
          id:order.length,
          usuarionnombre: email,
          nombre: usuarion.email,
          order: [],
          orederCost: 0,
          direccion: direccion,
          cel: cel,
          pago: 'Efectivo',
          estado: 'Pendiente'
      })
  };
  

const orderCostUpdate = (filter) => {
    const Prices = filter.order.map(u => u.precio * u.q);
    let totalOrder = 0;
    for (let i of Prices) totalOrder+=i;
    filter.orderCost = totalOrder
    return filter
}

const filterPP = (id, usuarion) => {
    const product = AllProductos().find(u => u.id == id);
    const filter = order.find(u => u.usuarionnombre == usuarion);
    return product, filter
}

const pago = [{id:1,method:'efectivo'},{id:2,method:'transferencia'},{id:3,method:'tarjeta'}]

const allpago = () => {
    return pago;
}

const newpago = (pago) => {
    const newpago =  {
        "id": pago [pago.length-1].id+1,
        "method": pago
    };
    return pago.push(newpago);
}

const changepago = (id,pago) => {
    const pagoFind = pago.find(u=> u.id == id);
    pagoFind.mode = pago;
    return pago;
};

const deletepago = (id) => {
    const pagoFind = pago.find(u=> u.id == id);
    pagos.splice(pagos.lastIndexOf(pagoFind),1);
    return pago;
};

const estados = [{id:1,mode:'Pendiente'},{id:2,mode:'Confirmado'},{id:3,mode:'Preparación'},{id:4,mode:'Enviado'},{id:5,mode:'Entregado'}];

const allestados  = () => {
    return estados
};

const changeestado = (id,estado) => {
    const estadoFind = estados.find( u=> u.id == id);
    estadoFind.mode = estado;
    return estadoFind
};

const deleteestado = (id) => {
    const estadoFind = estados.find(u => u.id == id);
    estados.splice(estados.lastIndexOf(estadoFind),1);
    return estados
};



module.exports = {allOrders, newOrder, usuarionUpdate,newusuarionModel, orderCostUpdate, filterPP, allpago, newpago, changepago, deletepago, allestados, changeestado, deleteestado}

