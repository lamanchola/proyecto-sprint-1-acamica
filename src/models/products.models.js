const producto = [
    {
        id:1,
        nombreproducto: "Empanada",
        precio: 2000,
        descripcion: "pasabocas frito relleno de pollo, carne o mixto.",
        q:5
    },
    {   
        id:2,
        nombreproducto: "Arepa",
        precio: 1500,
        descripcion: "pasabocas de maiz asado.",
        q:5
    },
    {   
        id:3,
        nombreproducto: "Sandwich",
        precio: 2500,
        descripcion: "pan blanco, jamon y queso. ",
        q:1
    },
    {   
        id:4,
        nombreproducto: "Pastel de pollo",
        precio: 3000,
        descripcion: "pasabocas horneado con masa de hojaldre y horneado.",
        q:1
    },
    {   
        id:5,
        nombreproducto: "Salchipapa",
        precio: 5000,
        descripcion: "papas a la francesa y salchicha, salsas al gusto.",
        q:1
    },
    {   
        id:6,
        nombreproducto: "Sandwich mamut",
        precio: 5000,
        descripcion: "pan baguette, lechuga, queso, jamon, pollo, tomate, cebolla, salsas al gusto.",
        q:1
    },
    {   
        id:7,
        nombreproducto: "jugo natural",
        precio: 2500,
        descripcion: "jugo en agua.",
        q:1
    },
    {   
        id:8,
        nombreproducto: "Botella de agua",
        precio: 2500,
        descripcion: "botella de agua mineral de 250ml.",
        q:1
    },
    {   
        id:9,
        nombreproducto: "Gaseosas surtidas",
        precio: 3000,
        descripcion: "botella de gaseosa plastica de 250ml.",
        q:1
    },
    {   
        id:10,
        nombreproducto: "Té",
        precio: 3000,
        descripcion: "botella con té de limo o durazno plastica de 250ml.",
        q:1
    }
];


const AllProductos = () => {
    return producto;
}

const pushProducto = (nombreProducto,precio,descripcion) => {
    const valId = Producto[Producto.length-1].id+1;
    const newProducto =    {   
        id:valId,
        nombreProducto: nombreProducto,
        precio: precio,
        descripcion: descripcion,
        q:1
    }
   producto.push(newProducto);
}

module.exports = {AllProductos, pushProducto}

