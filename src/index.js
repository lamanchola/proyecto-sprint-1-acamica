const express = require('express');
const app = express();
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const basicAuth = require('express-basic-auth');
app.use(express.json());

//Swagger
const swaggerOptions = require('./utils/swaggerOptions');
const swaggerSpecs = swaggerJsDoc(swaggerOptions);
app.use('/swagger', swaggerUI.serve, swaggerUI.setup(swaggerSpecs));

//Sing Up
app.use('/registro',require('./routers/account.route'))

//Routes
app.use('   ', require('./routers/admin.route'));
app.use('/usuario',require('./routers/user.route'));
app.use('/ordenes',require('./routers/orders.route'));
app.use('/productos',require('./routers/products.route'));

//Middlewares
const {autenticacion} = require('./middlewares/autenticacion.middleware');
app.use(basicAuth({authorizer: autenticacion}));

//Server
app.listen(3000, console.log('Escuchando puerto 3000'));
