const basicAuth = require('express-basic-auth');
const express = require('express');
const router = express.Router();
const {allOrders,newpago,allpago, allestados,deletepago} = require('../models/orders.models');
const {allusuario} = require('../models/users.models');
const {autentication} = require('../middlewares/autenticacion.middleware');
router.use(basicAuth({authorizer: autentication}));

//--------------------Gestión de Estados del Pedido (Solo administrador)--------------


//Middleware que verifica si es administrador el usuario logeado, solo puede acceder a estas rutas un administrador.
router.use('/', (req,res,next) => {
    if (allusuario().some(u => u.email === req.auth.user && u.isAdmin == true)== false) 
    return res.status(401).json('No está autorizado') 
    else return next() 
});

//Ver todos los pedidos solo para el administrador.
/**
 * @swagger
 * /administrador/orders:
 *      get:
 *          summary: Historial de ordenes de todos los usuarios.
 *          description: Ver todo el historial de ordenes.
 *          tags: [Gestión de pedidos]
 *          security:
 *              - basicAuth: []
 *          responses:
 *                  200:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */
router.get('/orders',(req,res)=>{
    res.status(200).json(allOrders())
});

//Permite ver todos los estados de pedidos.
/**
 * @swagger
 * /administrador/ordersstates:
 *      get:
 *          summary: Ver los estados de pedidos.
 *          description: Ver los estados para modificarlo.
 *          tags: [Gestión de pedidos]
 *          security:
 *              - basicAuth: []
 *          responses:
 *                  200:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */
router.get('/ordersstates',(req,res)=>{
    res.status(200).json(allestados())
});

//Modifica el estado del pedido, solo administrador.
/**
 * @swagger
 * /administrador//ordersstates/{id}/state/{idState}:
 *      put:
 *          summary: Modificar estado de orden confirmada.
 *          description: Cambia el estado de la orden
 *          tags: [Gestión de Ordenes]
 *          security:
 *              - basicAuth: []
 *          parameters:
 *            - in: path
 *              name: id
 *              description: id de la orden a seleccionar.
 *              required: true
 *              type: integer 
 *            - in: path
 *              name: idState
 *              description: id del Estado a relacionar a la orden seleccionada.
 *              required: true
 *              type: integer        
 *          responses:
 *                  '200':
 *                      description: Cambio exitoso de estado de la orden
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 *
 *                  400:
 *                      description: No se pudo cambiar el pedido.
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */

router.put('/ordersstates/:id/state/:idState',(req,res) => {
    const order = allOrders().find(u => u.id == req.params.id)
    const state = allestados().find(u => u.id == req.params.idState)
        if (order.state == "Pendiente") {
            res.status(400).json("No puede modificar su pedido pendiente hasta que el usuario confirme su orden.")
        } else {
            if (allOrders().some(u => u.id == req.params.id)){
                if (allestados().some(u => u.id == req.params.idState)){
                    if (state.id == 1 ){
                        res.json("No puede devolver el pedido a pendiente, el usuario ya lo ha confirmado.")
                    } else
                    order.state = state.mode
                    res.json(order)
                } else res.status(400).json("no existe")
            }else res.status(400).json("El pedido no existe")
        }
})

//-Gestión de medios de pago-

//Ver todos los medios de pago
/**
 * @swagger
 * /administrador/allpagomethods:
 *      get:
 *          summary: Todos los métodos de pago.
 *          description: Ver todos los métodos de pago.
 *          tags: [Métodos de Pago]
 *          security:
 *              - basicAuth: []
 *          responses:
 *                  201:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */
router.get('/allpagometodos', (req,res) => {
    res.status(200).json(allpago())
})

//Agrega un nuevo medio de pago
/**
 * @swagger
 * /administrador/newpagomethod/:
 *      post:
 *          summary: Agregar método de pago.
 *          description: Permitir agregar un nuevo método de pago con el id correspondiente.
 *          tags: [Métodos de Pago]
 *          security:
 *              - basicAuth: []
 *          requestBody:
 *              require: true
 *              content:
 *                  application/json:
 *                      schema:
 *                        $ref: '#/components/schemas/methodpage' 
 *                      type:
 *                          Array    
 *          responses:
 *                  200:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 *                  400:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */
router.post('/nuevometodo', (req,res) => {
    const {method} =req.body;
    if(allpago().some(u => u.method == method)){
       return res.status(400).send(' ya existe') 
    } else 
        newpago(method)
        res.status(200).json(allpago())
});


//Ver el método de pago
/**
 * @swagger
 * /administrador/pagomethod/{id}:
 *      get:
 *          summary: Ver método de pago.
 *          description: Ver el métodos de pago con el id .
 *          tags: [Métodos de Pago]
 *          security:
 *              - basicAuth: []
 *          parameters:
 *            - in: path
 *              name: id
 *              description: id del producto a agregar
 *              required: true
 *              type: integer  
 *          responses:
 *                  200:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 *                  400:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */
router.get('/pagomethod/:id', (req,res) => {
    const {id} = req.params;
    const  methodpago = allpago().find( u => u.id == id)
    if(allpago().some(u => u.id == id)) res.status(200).json(methodpago);
     else  res.status(400).json('no existe.')
});

//Modifica el método de pago.
/**
 * @swagger
 * /administrador/pagomethod/{id}:
 *      put:
 *          summary: Modificar método de pago.
 *          description: Permitir modificar el método de pago con el id.
 *          tags: [Métodos de Pago]
 *          security:
 *              - basicAuth: []
 *          parameters:
 *            - in: path
 *              name: id
 *              description: id del producto a modificar
 *              required: true
 *              type: integer
 *          requestBody:
 *              require: true
 *              content:
 *                  application/json:
 *                      schema:
 *                        $ref: '#/components/schemas/methodpage' 
 *                      type:
 *                          Array    
 *          responses:
 *                  200:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */
router.put('/pagomethod/:id', (req,res) => {
    const {id} = req.params
    const  methodpago = allpago().find( u => u.id == id)
    const {method} =req.body;
    if(allpago().some(u => u.id == id)){
        methodpago.method = method;
        res.status(200).json(allpago())
    } else  res.status(400).json('no existe.') 
  
})

//Elimina el método de pago
/**
 * @swagger
 * /administrador/pagomethod/{id}:
 *      delete:
 *          summary: Eliminar método de pago.
 *          description: Eliminar el métodos de pago con el id.
 *          tags: [Métodos de Pago]
 *          security:
 *              - basicAuth: []
 *          parameters:
 *            - in: path
 *              name: id
 *              description: id del producto a eliminar
 *              required: true
 *              type: integer  
 *          responses:
 *                  200:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */
router.delete('/pagomethod/:id', (req,res) => {
    const {id} = req.params
    if(allpago().some(u => u.id == id))res.status(200).json(deletepago(id));
    else res.status(400).json('El método de pago no existe.') 
});


// -----Schemas Swagger-----

/**
 * @swagger
 * name: Modificar o agregar método de pago
 * description: Formato para modificar método de pago
 * components:
 *  schemas:
 *      methodpage:
 *          type:   object
 *          required:
 *              -method
 *          properties:
 *              method:
 *                  type: string
 *                  example: efectivo
 *                  description: Ejemplo para método de pago.                  
 *          
 */
module.exports = router
