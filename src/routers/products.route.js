const basicAuth = require('express-basic-auth');
const express = require('express');
const router = express.Router();
const {allusuario} = require('../models/users.models');
const {AllProductos, pushProducto } = require('../models/products.models');
const {autentication} = require('../middlewares/autenticacion.middleware');
router.use(basicAuth({authorizer: autentication}));

//Middleware que verifica si es Admin el usuario logeado, solo puede acceder a estas rutas un Admin.
router.use('/', (req,res,next) => {
    
    const usuario=(allusuario().some(u => u.email === req.auth.user && u.isAdmin))
    if (usuario){
    return res.status(401).json('No está autorizado')}
    else return  next()
});

//Ver todos los productos. 
/**
 * @swagger
 * /productos:
 *      get:    
 *          summary: Ver todos los productos de restaurante
 *          descripcion: Trae todo todos los productos de restaurante
 *          tags: [Productos]
 *          security:
 *              - basicAuth: []
 *          responses:
 *                  200:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */

router.get('/', (req, res) => {
    console.log ("siiiiiiii")
    res.json(AllProductos())
});


//Ver el producto con este ID
/**
 * @swagger
 * /productos/{id}:
 *      get:    
 *          summary: Ver un producto en oferta en Dalilah Restó
 *          descripcion: Trae un producto en oferta en Dalilah Restó por id del producto.
 *          tags: [Productos]
 *          security:
 *              - basicAuth: []
 *          parameters:
 *            - in: path
 *              name: id
 *              descripcion: id del producto
 *              required: true
 *              type: integer    
 *          responses:
 *                  200:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */
router.get('/:id', (req, res) => {
    if (AllProductos().some(u => u.id == req.params.id)){
        const {id} = req.params;
        const filtro = AllProductos().find(u => u.id == id);
        res.json(filtro)
    } else res.json("El producto no existe")

});

//Crea un nuevo producto
/**
 * @swagger
 * /productos:
 *      post:    
 *          summary: Agregar producto para oferta en Dalilah Restó
 *          descripcion: Agrega un producto para oferta en Dalilah Restó por id del producto.
 *          tags: [Productos]
 *          security:
 *              - basicAuth: []
 *          requestBody:
 *              require: true
 *              content:
 *                  application/json:
 *                      schema:
 *                        $ref: '#/components/schemas/products' 
 *                      type:
 *                          Array  
 *          responses:
 *                  200:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 *                  400:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */
router.post('/', (req, res) => {
    const {nombreproducto,precio,descripcion} = req.body
    if (AllProductos().some(u => u.nombreproducto == nombreproducto)){
        return res.status(400).json("El producto ya existe en la oferta.")}
    else {
        if (nombreproducto && precio&& descripcion) {
            pushProducto(nombreproducto,precio,descripcion)
            res.status(201).json(AllProductos());   
        } else return res.status(400).json("Datos incompletos, debe ingresar un nombre de producto, precio y descripción" ) 
    }
    

})

//Modificar el producto.
/**
 * @swagger
 * /productos/{id}:
 *      put:    
 *          summary: Modifica un producto en oferta en Dalilah Restó
 *          descripcion: Modifica un producto en oferta en Dalilah Restó por id del producto.
 *          tags: [Productos]
 *          security:
 *              - basicAuth: []
 *          parameters:
 *            - in: path
 *              name: id
 *              descripcion: id del producto a modificar
 *              required: true
 *              type: integer  
 *          requestBody:
 *              require: true
 *              content:
 *                  application/json:
 *                      schema:
 *                        $ref: '#/components/schemas/products' 
 *                      type:
 *                          Array   
 *          responses:
 *                  200:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 *                  400:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */
router.put('/:id', (req, res) => {
    const  product = AllProductos().find( u => u.id == req.params.id);
    const {nombreproducto, precio,descripcion} = req.body;
    if (!AllProductos().some( u => u.id == req.params.id)){
        return res.status(400).json("El producto no existe en la oferta.")
    } else {
        if (nombreproducto && precio&& descripcion){
            product.nombreproducto = nombreproducto;
            product.precio = precio;
            product.descripcion = descripcion;
            res.json(AllProductos());
        } else return res.status(400).json("Datos incompletos, debe ingresar un nombre de producto, precio y descripción" )    
    }
});

//Elimina un producto.
/**
 * @swagger
 * /productos/{id}:
 *      delete:    
 *          summary: Elimina un producto en oferta en Dalilah Restó
 *          descripcion: Elimina un producto en oferta en Dalilah Restó por id del producto.
 *          tags: [Productos]
 *          security:
 *              - basicAuth: []
 *          parameters:
 *            - in: path
 *              name: id
 *              descripcion: id del producto a eliminar.
 *              required: true
 *              type: integer    
 *          responses:
 *                  200:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */
router.delete('/:id', (req, res) => {
    if (!AllProductos().some(u => u.id== req.params.id)){
        return res.status(400).json("El producto no existe en la oferta.")}
    else {
        const  product = AllProductos().find( u => u.id == req.params.id);
        AllProductos().splice(AllProductos().lastIndexOf(product),1);
        res.status(200).json( AllProductos());
        console.log('Producto eliminado')
    }
});


// -----Schemas Swagger-----


/**
 * @swagger
 * name: Agregar o Modificar Productos
 * descripcion: Modelo para añadir o modificar  productos.
 * components:
 *  schemas:
 *      products:
 *          type: object
 *          required:
 *              -nombreproducto
 *              -precio
 *              -descripcion
 *          properties:
 *              nombreproducto:
 *                  type: string
 *                  example: string
 *                  descripcion: nombre del producto
 *              precio:
 *                  type: number
 *                  example: number
 *                  descripcion: precio del producto
 *              descripcion:
 *                  type: string
 *                  example: string
 *                  descripcion: nombre del producto
 * 
 */


module.exports = router
