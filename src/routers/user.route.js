const basicAuth = require('express-basic-auth');
const express = require('express');
const router = express.Router();
const {allusuario, pushusuario, putusuario} = require('../models/users.models');
const {newusuarionModel} = require('../models/orders.models');
const {autentication} = require('../middlewares/autenticacion.middleware');
router.use(basicAuth({authorizer: autentication}));




//Middleware que verifica si es Administrador el usuario logeado, solo puede acceder a estas rutas un Administrador.
router.use('/',(req,res,next) => {
    
const usuario=(allusuario().some(u => u.email === req.auth.user && u.isAdmin))
    console.log (usuario)
    if (usuario){
    return next()}
    else return res.status(401).json('No está autorizado') 
    
}); 

//Trae todos los usuarios, solo Admin.
/**
 * @swagger
 * /usuarios:
 *      get:
 *          summary: Ver todos los usuarios.
 *          description: Ver todos los usuarios registrados en Delilah Restó.
 *          tags: [Usuarios]
 *          security:
 *              - basicAuth: []
 *          responses:
 *                  201:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */
router.get('/', (req, res) => {
     res.json(allusuario())
});

//Trae id del usuario en Params, solo Admin.
/**
 * @swagger
 * /usuarios/{id}:
 *      get:
 *          summary: Ver usuario por ID.
 *          description: Ver un solo usuario registrado  por su id.
 *          tags: [Usuarios]
 *          security:
 *              - basicAuth: []
 *          parameters:
 *            - in: path
 *              nombre: id
 *              description: id del usuario
 *              required: true
 *              type: integer 
 *          responses:
 *                  201:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */
router.get('/:id',(req, res) => {
    const {id} = req.params;
    const filtro = allusuario().find(u => u.id == id)
    res.json(filtro)
});

//Crea Usuario desde el usuario Admin.
/**
 * @swagger
 * /usuarios:
 *      post:
 *          summary: Agregar usuario.
 *          description: Agregar usuario por medio de Admin en la api.
 *          tags: [Usuarios]
 *          security:
 *              - basicAuth: []
 *          requestBody:
 *              required: true
 *              content:
 *                  application/json:
 *                     schema:
 *                          $ref: '#/components/schemas/crearcuenta'
 *                     type: 
 *                          Array 
 *          responses:
 *                  201:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 *                  400:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */
router.post('/',(req, res) => {
    const {email, nombre, cel, direccion,password} = req.body;
    const findEmail = allusuario().some(u => u.email === email );
    if (email && nombre && cel && direccion && password) {
        if (findEmail == false){
                pushusuario(email,nombre, cel, direccion, password);
                newusuarionModel(email,direccion,cel)
                res.status(201).json('cuenta creada')
        } else return res.status(400).json('El email ya existe, por favor ingrese uno nuevo') 
    }else return res.status(400).json('Requerimientos incompletos')
    
});

//Modifica un usuario desde Administrador.
/**
 * @swagger
 * /usuarios/{id}:
 *      put:
 *          summary: Modificar usuario por ID.
 *          description: Modificar un solo usuario registrado por id.
 *          tags: [Usuarios]
 *          security:
 *              - basicAuth: []
 *          parameters:
 *            - in: path
 *              nombre: id
 *              description: id del usuario a modificar
 *              required: true
 *              type: integer 
 *          requestBody:
 *              required: true
 *              content:
 *                  application/json:
 *                     schema:
 *                          $ref: '#/components/schemas/crearcuenta'
 *                     type: 
 *                          Array
 *          responses:
 *                  200:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 *                  400:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */
router.put('/:id', (req, res) => {
    const {email, nombre, cel, direccion,password} = req.body;
    if (allusuario().some( u => u.id == req.params.id)) {
        if (email && nombre && cel && direccion && password) {
            putusuarios(req.params.id,email,nombre, cel, direccion, password);
            res.json(allusuario().find(u => u.id == req.params.id));
        } else return res.status(400).json('Requerimientos incompletos')   
    } else return res.status(400).json('El usuario con este id no existe.')
        
        
    
});

//Elimina el usuario con id del params, solo Administrador. 
/**
 * @swagger
 * /usuarios/{id}:
 *      delete:
 *          summary: Eliminar usuario por ID.
 *          description: Eliminar un solo usuario registrado por id.
 *          tags: [Usuarios]
 *          security:
 *              - basicAuth: []
 *          parameters:
 *            - in: path
 *              nombre: id
 *              description: id del usuario
 *              required: true
 *              type: integer 
 *          responses:
 *                  201:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */
router.delete('/:id', (req, res) => {
    const user = allusuario().find(u => u.id == req.params.id);
    if(!allusuario().some(u => u.id == req.params.id)){
        return res.status(400).json("El usuario con el id indicado no existe")
    } else {
        if(user.isAdmin == false){
            allusuario().splice(allusuario().lastIndexOf(user),1);
            res.json(allusuario());
            console.log('Usuario eliminado')
        } else return res.status(400).json("No puedes eliminar el usuario administrador")
    }
   
});


// -----Schemas Swagger-----

/**
 * @swagger
 * nombre: Registo de usuario
 * description: formato para crear usuario.
 * components:
 *  schemas:
 *      crearusuario:
 *          type:   object
 *          required:
 *              -email
 *              -nombre
 *              -cel
 *              -direccion
 *              -password
 *          properties:
 *              email:
 *                  type: string
 *                  example: ejemplo@gmail.com
 *                  description: correo electrónico del usuario
 *              nombre:
 *                  type: string
 *                  example: Camila
 *                  description: nombres y apellidos del usuario
 *              cel:
 *                  type: number
 *                  example: 3124398310
 *                  description: numero celular
 *              direccion:
 *                  type: string
 *                  example: cra 7 #2-99 sur
 *                  description: direccion del usuario.
 *              password:
 *                  type: string
 *                  example: 12345
 *                  description: contrasenia de acceso
 *          
 *                  
 */

module.exports = router
