const express = require('express');
const router = express.Router();
const {allusuario, pushusuario} = require('../models/users.models');
const {newusuarionModel} = require('../models/orders.models');

//Mensaje bienvenida para el usuario.
/**
 * @swagger
 * /registro:
 *      get:
 *          summary: Bienvenido
 *          description: Ver mensaje de bienvenida.
 *          tags: [Registro]
 *          security: []
 *          responses:
 *                  200:
 *                      description: usuario creado
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 *
 *                  400:
 *                      description: usuario no creado
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */

router.get('/', (req,res) => {
    res.status(200).send('Registrate hacer pedidos desde nuestro restaurante')
})

//Cuando se crea un usuario se crea pedido vacío.
/**
 * @swagger
 * /registro:
 *      post:
 *          summary: Creación de un usuario.
 *          description: creacion de un usuario para hacer pedidos
 *          tags: [Registro]
 *          security: []
 *          requestBody:
 *              required: true
 *              content:
 *                  application/json:
 *                     schema:
 *                          $ref: '#/components/schemas/crearcuenta'
 *                     type: 
 *                          Array             
 *          responses:
 *                  '201':
 *                      description: creacion exitosa de la cuenta
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 *
 *                  400:
 *                      description: cuenta no creada
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */
router.post('/', (req,res) => {
    const {email, nombre, cel, direccion,password} = req.body;
    const findEmail = allusuario().some(u => u.email === email );
    if (email && nombre && cel && direccion && password) {
        if (findEmail == false){
                pushusuario(email,nombre, cel, direccion, password);
                newusuarionModel(email,direccion,cel);
                res.status(201).json('cuenta creada')
          
        } else return res.status(400).json('El email ya existe, por favor ingrese otro') 
    }else return res.status(400).json('Requerimientos incompletos')
         
});

//El usuario puede logearse/acceder a su cuenta desde este login.
/**
 * @swagger
 * /registro/login:
 *      post:
 *          summary: Acceder a su cuenta en Delilah Restó.
 *          description: acceso a cuenta para hacer pedidos
 *          tags: [Registro]
 *          security: []
 *          requestBody:
 *              required: true
 *              content:
 *                  application/json:
 *                     schema:
 *                          $ref: '#/components/schemas/login'
 *                     type: 
 *                          Array             
 *          responses:
 *                  '200':
 *                      description: acceso exitosa q su cuenta
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 *
 *                  400:
 *                      description: no fue exitoso el cceso a la cuenta
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */

router.post('/login',   (req, res) => {
    const {email, password} = req.body;
    if (email && password) {
        if (allusuario().some( u => u.email === email && u.password === password)){
            res.status(200).send('exitoso ya estas logeado')
        } else return res.status(400).send('correo o contraseña incorrecto')
    }else return res.status(400).send('Requerimientos incompletos')
})

// -----Schemas Swagger-----

/**
 * @swagger
 * nombre: Registo de usuario
 * description: formato para crear cuenta.
 * components:
 *  schemas:
 *      crearcuenta:
 *          type:   object
 *          required:
 *              -email
 *              -nombre
 *              -cel
 *              -direccion
 *              -password
 *          properties:
 *              email:
 *                  type: string
 *                  example: ejemplo@gmail.com
 *                  description: correo electrónico del usuario
 *              nombre:
 *                  type: string
 *                  example: Camila Manchola
 *                  description: nombres y apellidos del usuario
 *              cel:
 *                  type: number
 *                  example: 3124398310
 *                  description: numero celular
 *              direccion:
 *                  type: string
 *                  example: calle 123 # 99-01
 *                  description: direccion de entrega.
 *              password:
 *                  type: string
 *                  example: 12345
 *                  description: contrasenia para el acceso 
 *          
 *                  
 */

/**
 * @swagger
 * nombre: Registo de usuario
 * description: formato para crear cuenta.
 * components:
 *  schemas:
 *      login:
 *          type:   object
 *          required:
 *              -email
 *              -password
 *          properties:
 *              email:
 *                  type: string
 *                  example: ejemploa@gmail.com
 *                  description: correo electrónico del usuario
 *              password:
 *                  type: string
 *                  example: 12345
 *                  description: contrasenia  del usuario
 *          
 *                  
 */

module.exports = router
